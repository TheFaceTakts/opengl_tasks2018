#include "Mesh.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

glm::vec3 getBottlePont(float r, float nu, float theta)
{
    return glm::vec3((r + cos(theta / 2) * sin(nu) - sin(theta / 2) * sin(2 * nu)) * cos(theta),
                     (r + cos(theta / 2) * sin(nu) - sin(theta / 2) * sin(2 * nu)) * sin(theta),
                     sin(theta / 2) * sin(nu) + cos(theta / 2) * sin(2 * nu));
}

glm::vec3 getBottleNormal(float r, float nu, float theta) {
    glm::vec3 r_nu = glm::vec3((cos(theta / 2) * cos(nu) - 2 * sin(theta / 2) * cos(2 * nu)) * cos(theta),
                               (cos(theta / 2) * cos(nu) - 2 * sin(theta / 2) * cos(2 * nu)) * sin(theta),
                               (2 * cos(theta / 2) * cos(2 * nu) + sin(theta / 2) * cos(nu)));
    glm::vec3 r_theta = glm::vec3((-0.5 * sin(theta / 2) * sin(nu) - 0.5 * cos(theta / 2) * sin(2 * nu)) * cos(theta) -
                                  (r + cos(theta / 2) * sin(nu) - sin(theta / 2) * sin(2 * nu)) * sin(theta),
                                  (-0.5 * sin(theta / 2) * sin(nu) - 0.5 * cos(theta / 2) * sin(2 * nu)) * sin(theta) +
                                  (r + cos(theta / 2) * sin(nu) - sin(theta / 2) * sin(2 * nu)) * cos(theta),
                                  0.5 * cos(theta / 2) * sin(nu) - 0.5 * sin(theta / 2) * sin(2 * nu));
    glm::vec3 res = glm::cross(r_nu, r_theta);
    glm::normalize(res);
    return -res;
}

MeshPtr makeBottle(float radius, unsigned int N)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (unsigned int i = 0; i < N; i++)
    {
        float theta = 2.0f * glm::pi<float>() * i / N;
        float theta1 = 2.0f * glm::pi<float>() * (i + 1) / N;

        for (unsigned int j = 0; j < N; j++)
        {
            float nu = 2.0f * glm::pi<float>() * j / N;
            float nu1 = 2.0f * glm::pi<float>() * (j + 1) / N;

            vertices.push_back(getBottlePont(radius, nu, theta));
            vertices.push_back(getBottlePont(radius, nu1, theta));
            vertices.push_back(getBottlePont(radius, nu, theta1));

            normals.push_back(getBottleNormal(radius, nu, theta));
            normals.push_back(getBottleNormal(radius, nu1, theta));
            normals.push_back(getBottleNormal(radius, nu, theta1));

            vertices.push_back(getBottlePont(radius, nu1, theta));
            vertices.push_back(getBottlePont(radius, nu, theta1));
            vertices.push_back(getBottlePont(radius, nu1, theta1));

            normals.push_back(getBottleNormal(radius, nu1, theta));
            normals.push_back(getBottleNormal(radius, nu, theta1));
            normals.push_back(getBottleNormal(radius, nu1, theta1));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());


    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


