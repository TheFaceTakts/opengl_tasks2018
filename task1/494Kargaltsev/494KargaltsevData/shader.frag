/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 330

uniform bool white;

in vec4 color;

out vec4 fragColor;

void main()
{
    if (white) {
        fragColor = vec4(1.0, 1.0, 1.0, 1.0);
    } else {
        fragColor = color;
    }
}
