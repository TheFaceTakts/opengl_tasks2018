#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include <iostream>

class SampleApplication : public Application {
public:
    MeshPtr _bottle;

    ShaderProgramPtr _shader;

    bool _show_grid = false;
    bool _grid_colored = false;
    bool _show_surface = true;

    unsigned int _N = 30;

    const unsigned int kMaxn = 400;

    const unsigned int kMinn = 5;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _bottle = makeBottle(2.f, _N);

        _bottle->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("494KargaltsevData/shaderNormal.vert",
                                                  "494KargaltsevData/shader.frag");
    }

    void updateBottle() {
        auto mm = _bottle->modelMatrix();
        _bottle = makeBottle(2.f, _N);
        _bottle->setModelMatrix(mm);
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_MINUS)
            {
                _N /= 1.2;
                _N = std::max(kMinn, _N);

            }
            else if (key == GLFW_KEY_EQUAL && (mods & GLFW_MOD_SHIFT))
            {
                _N *= 1.2;
                _N = std::min(kMaxn, _N);
            }
            updateBottle();
        }
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
        {
            if (_bottle) {
                ImGui::Text("Number of vertices: %d", _bottle->getVertexCount());
                ImGui::Checkbox("Show grid", &_show_grid);
                ImGui::Checkbox("Grid colored", &_grid_colored);
                ImGui::Checkbox("Show surface", &_show_surface);
            }
        }
        ImGui::End();
    }

    void drawGUI() override {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        Application::drawGUI();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем первый меш
        _shader->setMat4Uniform("modelMatrix", _bottle->modelMatrix());

        if (_show_grid) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            _shader->setBoolUniform("white", !_grid_colored);
            _bottle->draw();
        }
        if (_show_surface) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            _shader->setBoolUniform("white", false);
            _bottle->draw();
        }
    }
};

int main() {
    SampleApplication app;
    app.start();

    return 0;
}